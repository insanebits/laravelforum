<?php

namespace Code4coffee\DynamicConfiguration;

use Illuminate\Support\Facades\Facade;

use Illuminate\Cache\CacheManager as Cache;

use Illuminate\Database\QueryException;

class DynamicConfiguration extends Facade
{
    const CACHE_PREFIX = 'dynamic_configuration_';
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'DynamicConfiguration'; }

    public static function get($key, $default = null)
    {
        $value = $default;

        if(self::isCached($key))
        {
            $value = self::getCached($key);
        } else {
            try {
                $dbValue = self::getDatabaseValue($key);
            } catch(\Exception $e) {
                $dbValue = $default;
            }


            if($dbValue !== false)
            {
                $value = $dbValue;

                // cache value
                self::addCache($key, $value);
            }
        }

        return $value;
    }

    private static function isCached($key)
    {
        return Cache::has(self::CACHE_PREFIX, $key);
    }

    private static function getDatabaseValue($key)
    {
        $dbRecord = \Setting::where('setting', $key)->first();

        if($dbRecord->exists)
        {
            return $dbRecord->value;
        }

        return false;
    }

    private static function addCache($key, $value)
    {
        return Cache::add(self::CACHE_PREFIX . $key, $value);
    }
}
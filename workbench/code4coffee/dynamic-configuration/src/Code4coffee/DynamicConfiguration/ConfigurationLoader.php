<?php

namespace Code4coffee\DynamicConfiguration;

class ConfigurationLoader extends \Illuminate\Config\FileLoader {
    private $dynamicallityIndicator = 'dynamic-setting';

    /**
     * {@inheritDoc}
     */
    public function load($environment, $group, $namespace = null)
    {
        $items = parent::load($environment, $group, $namespace);

        return array_replace_recursive($items, $this->overrideConfiguration($items));
    }

    /**
     * Override loaded configuration with dynamic values from database
     *
     * @param $settings
     * @return mixed
     */
    private function overrideConfiguration($settings)
    {
        foreach($settings as $name => $value) {
            if($this->isDynamic($name, $value))
            {
                $settings[$name] = $this->getDynamicValue($name, $this->getDefaultValue($value));
            }
        }

        return $settings;
    }

    private function isDynamic($name, $value)
    {
        return is_string($value) && strpos($value, $this->dynamicallityIndicator) === 0;
    }

    /**
     * Extracts dynamic setting default value
     *
     * default value needs to be separated with pipe to parse it correctly
     * For example: dynamic-setting|default
     *
     * @param $value
     * @return null
     */
    private function getDefaultValue($value)
    {
        $parts = explode('|', $value);

        return isset($parts[1]) ? $parts[1] : null;
    }

    private function getDynamicValue($name, $default)
    {
        $dbRecord = Setting::where('setting', $name)->first();

        if($dbRecord && $dbRecord->exists)
        {
            return $dbRecord->value;
        }

        return $default;
    }
}
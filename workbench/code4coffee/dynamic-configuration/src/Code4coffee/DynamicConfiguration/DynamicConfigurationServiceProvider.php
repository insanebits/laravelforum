<?php namespace Code4coffee\DynamicConfiguration;

use Illuminate\Support\ServiceProvider;

class DynamicConfigurationServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * Will be called when application is booted
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('code4coffee/dynamic-configuration');

		$loader = new ConfigurationLoader($this->app['files'], $this->app['path'] . '/config');

		$this->app['config']->setLoader($loader);

		foreach (array_keys($this->app['config']->getItems()) as $collection)
		{
			unset($this->app['config'][$collection]);
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// Register 'underlyingclass' instance container to our UnderlyingClass object
		$this->app['DynamicConfiguration'] = $this->app->share(function($app)
		{
			return new DynamicConfiguration;
		});

		// Shortcut so developers don't need to add an Alias in app/config/app.php
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('DynamicConfiguration', 'Code4coffee\DynamicConfiguration\DynamicConfiguration');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}

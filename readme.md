## Laravel Forum

Simple to use forum system, written using modern Laravel PHP framework. Main focus is in user experience while maintaining development workflow as simple as possible.

Currently in development, far from v1.

### Features
 
 * Visitors can access forum and read answers
 * Dynamic configuration
 * Separate administration panel
 * Plugin support via composer packages
 * Theme support

### TODO for V1

There is features which needs fixing or implementation  

 * Register firstname and lastname
 * Plugin hooks
 * Unit Testing
 * Sitemaps
 * Set user group
 * Widget management
 * Plugin management
 * Theme management
 * Settings management
 * ACL management
 * Menu management
 * Markdown syntax support
 * Page creation logic and page types
 * Installer
 * Theme styling
<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Website title
    |--------------------------------------------------------------------------
    |
    | This is used when showing website title for user, it will be appended to
    | every page and separated with pipe as recommended for SEO purposes
    */
    'website_title' => 'dynamic-setting|Laravel-Forum Administrative Panel',

    /*
    |--------------------------------------------------------------------------
    | Administrative Panel folder name
    |--------------------------------------------------------------------------
    |
    | Security setting to change where administration panel is located
    |
    */
    'admin_folder' => 'dynamic-setting|admin',

    /*
    |--------------------------------------------------------------------------
    | Backend Theme
    |--------------------------------------------------------------------------
    |
    | Theme is the styling of the template, it's supposed to deal with aesthetic side of the site
    | It will use Assets, CSS and Javascript
    |
    */
    'theme' => 'dynamic-setting|default',

    /*
    |--------------------------------------------------------------------------
    | Backend Template
    |--------------------------------------------------------------------------
    |
    | Template is a collection of views which basically lays out elements which later are styled using themes,
    | Single template may have multiple themes available to choose from
    */
    'template' => 'dynamic-setting|default'
);
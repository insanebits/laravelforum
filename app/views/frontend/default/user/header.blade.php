@extends('frontend.layout')

@section('user_profile')
    <div class="pull-right">
        <ul class="nav navbar-nav">
            <li>{{ link_to(route('user-profile'), $user->username) }}</li>
            <li>{{ link_to('logout', 'Logout') }}</li>
        </ul>
    </div>
@stop
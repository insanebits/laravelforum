@extends('frontend.layout')
@section('content')
    <h2>{{ sprintf(Lang::get('User %s profile'), $user->username) }}</h2>
@stop
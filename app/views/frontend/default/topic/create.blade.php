@section('content')
    <div class="row">
    <h2>
         @lang('Create topic')
    </h2>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <div >

            {{ Form::open(array('url' => 'topic/create')) }}
            <div class="form-group">
                {{ Form::label('Name', null) }}
                {{ Form::text('name', null,  array('class' => 'form-control')) }}
            </div>


            <div class="form-group">
                <div>{{ Form::label('Question') }}</div>
                <div>{{ Form::textarea('question', null,  array('class' => 'form-control')) }}</div>
            </div>

            <div class="form-group">
                <div>{{ Form::label('Tags') }}</div>
                <div>{{ Form::text('tags', null, array('class' => 'form-control')) }}</div>
            </div>

            <div class="form-group">
                {{ Form::submit('save',  array('class' => 'btn btn-default')) }}
            </div>
            {{ Form::close() }}
        </div>

    </div>
@append
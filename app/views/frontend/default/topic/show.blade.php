@extends('frontend.layout')

@section('content')
    <div class="row">
        <div class="col-lg-1 topic-score">
            {{ link_to(route('topic-upvote', array($topic->id)), '', array('class' => 'upvote glyphicon glyphicon-chevron-up')) }}
            <span class="value">{{ $topic->score() }}</span>
            {{ link_to(route('topic-downvote', array($topic->id)), '', array('class' => 'downvote glyphicon glyphicon-chevron-down')) }}
        </div>

        <div class="col-lg-11">
            <h1 class="topic-title">{{{ $topic->title }}}</h1>

            <p>{{{ $topic->content }}}</p>

            <div>
                @foreach($topic->tags()->get() as $tag)
                    {{ link_to(route('tag-view', array($tag->slug)), $tag->title) }}
                @endforeach
            </div>
        </div>
        <div class="col-lg-12">
            <h2>@lang('Answers:')</h2>
        </div>

        <div class="answer-list">
            @foreach($answers as $answer)
                <div class="answer">
                    <div class="answer-score col-lg-1">
                        {{ link_to(route('topic-answer-upvote', array($answer->id)), '', array('class' => 'upvote glyphicon glyphicon-chevron-up')) }}
                        <span class="value">
                        {{ $answer->score() }}
                    </span>
                        {{ link_to(route('topic-answer-downvote', array($answer->id)), '', array('class' => 'downvote glyphicon glyphicon-chevron-down')) }}
                    </div>
                    <div class="answer-content col-lg-11">
                        <p>{{{ $answer->content }}}</p>

                        <div style="float:right;">
                            @lang('answered by ') {{ $answer->author->username }}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>

        <div class="col-lg-12">
            <h3>@lang('Your answer')</h3>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    </div>
    <div class="col-lg-12">

        {{ Form::open(array('url' => 'topic/answer')) }}
        {{ Form::hidden('topic_id', $topic->id) }}

        <div class="form-group">
            <div>{{ Form::textarea('content', null, array('class' => 'form-control')) }}</div>
        </div>
        <div class="form-group">
            {{ Form::submit('save', array('class' => 'btn btn-default')) }}
        </div>

        {{ Form::close() }}
    </div>
    </div>
@stop
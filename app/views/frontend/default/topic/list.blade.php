@extends('frontend.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h2>@yield('title', 'Latest topics')</h2>
        </div>
        <div class="col-lg-12">
            <div class="pull-right">
                {{ link_to(route('topic-form'), 'Create topic') }}
            </div>

        </div>

        @if(!$topics->isEmpty())
            @foreach($topics as $topic)
                <div class="topic">
                    <div class="topic-info col col-lg-1 col-sm-2 col-xs-3">
                        <div class="topic-anwers">
                            <span class="big topic-answer-count">{{ $topic->answers->count() }}</span>
                            <span class="small">@lang('answers')</span>
                        </div>

                        <div class="topic-score">
                            {{ $topic->score() }} @lang('votes')
                        </div>
                    </div>
                    <div class="topic-details col-lg-11 col-sm-10 col-xs-9">
                        <h3 class="topic-title">
                            {{ link_to('topics/' . $topic->id . '/' . $topic->slug, $topic->title) }}
                        </h3>
                        <p class="topic-excerpt">
                            {{ $topic->excerpt() }} ...
                        </p>

                        <div class="topic-tags">
                            @foreach($topic->tags()->get() as $tag)
                                <span class="tag">
                                    {{ $tag->title }}
                                </span>
                            @endforeach
                        </div>

                        <div class="topic-author">
                            @lang('Asked by') {{ link_to('user/' . $topic->author()->first()->username, $topic->author()->first()->username) }}
                        </div>
                    </div>

                </div>
            @endforeach
        @else
            <div class="topic">
                @lang('No topics yet')
            </div>
        @endif
    </div>
@append
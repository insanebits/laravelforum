@extends('frontend.layout')

@section('content')
    <h2>@lang('Popular tags')</h2>

    <div>
        <ul>
            @if(!$tags->isEmpty())
                @foreach($tags as $tag)
                    <li>{{ link_to(route('tag-view', array($tag->slug)), $tag->title) }}</li>
                @endforeach
            @else
                <li>
                    @lang('No tags yet')
                </li>
            @endif
        </ul>
    </div>
@append
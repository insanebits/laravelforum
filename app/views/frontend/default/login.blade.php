@extends('frontend.layout')

@section('content')

<div class="row">
    @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="col-lg-4 col-lg-offset-4">
        <h2>@lang('Login')</h2>
            {{ Form::open() }}
            <div class="form-group">
                <div>{{ Form::label('Username') }}</div>
                <div>{{ Form::text('username', null, array('class' => 'form-control')) }}</div>
            </div>
            <div class="form-group">
                <div>{{ Form::label('Password') }}</div>
                <div>{{ Form::password('password', array('class' => 'form-control')) }}</div>
            </div>
            <div class="form-group">
                {{ Form::submit('login', array('class' => 'btn btn-default')) }}
            </div>
            {{ Form::close() }}
    </div>
</div>
@stop
@extends('frontend.layout')


@section('user_profile')
    <div class="pull-right">
        <ul class="nav navbar-nav">
            <li>{{ link_to('register', 'Sign up') }}</li>
            <li>{{ link_to('login', 'Login') }}</li>
        </ul>
    </div>
@stop

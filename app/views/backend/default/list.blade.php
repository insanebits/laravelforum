@extends('backend.layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">

            <div class="box-header">
                <h3 class="box-title">{{ isset($list_title) ? $list_title : '' }}</h3>

                @if(isset($search) && $search)
                    <div class="box-tools">
                        {{ Form::open(array('url' => Request::path(), 'method' => 'get')) }}
                        <div class="input-group">
                            <input type="text" placeholder="Search" style="width: 150px;" class="form-control input-sm pull-right" name="search" value="{{{ Input::get('search') }}}">
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                @endif

                <div class="pull-right">
                    <h3 class="box-title">
                        {{ isset($create_link) ? $create_link : '' }}
                    </h3>
                </div>
            </div>
            {{-- form opens on box body because search will be submitted via get actions will be posted --}}
            {{ Form::open() }}
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        @if(isset($bulk_actions) && !empty($bulk_actions))
                            <th>#</th>
                        @endif

                        @foreach($columns as $att)
                            @if($att['visible'])
                                <th>
                                    {{ $att['label'] }}
                                </th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $node)
                        <tr>
                            @if(isset($bulk_actions) && !empty($bulk_actions))
                                <td>{{ Form::checkbox('bulk_selection[]', $node['id']) }}</td>
                            @endif
                            @foreach($columns as $att)
                                @if($att['visible'])
                                    <td> {{ $node[$att['name']] }}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="box-footer clearfix">
                <div class="pull-right col-xs-2">
                    @foreach($bulk_actions as $action => $enabled)
                        @if($enabled)
                            <button name="bulk_action"
                                    {{ in_array($action, array('delete', 'delete_trashed')) ? 'onClick="if(confirm(\'Are you really want to delete?\') == false) return false;"' : ''}}
                                    class="btn btn-flat pull-right {{ in_array($action, array('delete', 'delete_trashed')) ? 'btn-danger' : 'btn-default'}}"
                                    value="{{{ $action }}}">
                                {{ ucwords(str_replace('_', ' ', $action)) }}
                            </button>
                        @endif
                    @endforeach
                </div>

                <div class="pull-left col-xs-6">
                    {{ $pagination }}
                </div>
            </div>
            {{ Form::close() }}



        </div>
    </div>
</div>

@if(isset($children))
    @foreach($children as $child)
        {{$child}}
    @endforeach
@endif

@stop
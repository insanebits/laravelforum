<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// create groups before creating users
		$this->seedGroups();

		$this->seedUsers();
	}

	public function seedGroups()
	{
		$groups = array('admin', 'registered', 'moderator');

		foreach($groups as $name) {
			$group = new \Group();
			$group->name = $name;

			$group->save();
		}
	}

	public function seedUsers()
	{
		$admin = new User();

		$admin->username = 'admin';
		$admin->password = \Hash::make('changeit');

		$admin->save();

		// add to admin group

		$group = Group::where('name', 'admin')->first();

		$admin->groups()->attach($group->id);
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($t){
			/** var Illuminate\Database\Schema\Blueprint $t */
			$t->increments('id');
			$t->text('username');
			$t->text('firstname');
			$t->text('lastname');
			$t->text('password');
			$t->text('email');
			$t->text('remember_token');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

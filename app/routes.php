<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', array('as' => 'home', 'uses' => 'Frontend\TopicController@latestTopics'));
Route::get('topics/{id}/{slug}', array('as' => 'topic-view', 'uses' => 'Frontend\TopicController@showTopic'));
// tag view
Route::get('tag/{slug}', array('as' => 'tag-view', 'uses' => 'Frontend\TagController@showTag'));
// popular tags
Route::get('tags', array('as' => 'tag-list', 'uses' => 'Frontend\TagController@tagsList'));

Route::any('login', 'Frontend\LoginController@login');
Route::get('logout', 'Frontend\LoginController@logout');

Route::match(['GET', 'POST'], 'register', array('as' => 'register', 'uses' => 'Frontend\LoginController@register'));


Route::group(array('prefix' => 'topic', 'before' => 'auth'), function(){
    // maybe topic/ask route would be more logical
    Route::post('answer', array('as' => 'topic-answer', 'uses' => 'Frontend\TopicController@answerTopic'));
    Route::match(['GET', 'POST'], 'create', array('as' => 'topic-form', 'uses' => 'Frontend\TopicController@createTopic'));

    // ajax should be added later
    Route::get('answer/{id}/upvote', array('as' => 'topic-answer-upvote', 'uses' => 'Frontend\TopicController@upvoteAnswer'));
    Route::get('answer/{id}/downvote', array('as' => 'topic-answer-downvote', 'uses' => 'Frontend\TopicController@downvoteAnswer'));

    Route::get('{id}/upvote', array('as' => 'topic-upvote', 'uses' => 'Frontend\TopicController@upvoteTopic'));
    Route::get('{id}/downvote', array('as' => 'topic-downvote', 'uses' => 'Frontend\TopicController@downvoteTopic'));
});

// logged in user routes
Route::group(array('prefix' => 'user', 'before' => 'auth'), function(){
    Route::get('profile', array('as' => 'user-profile', 'uses' => 'Frontend\ProfileController@privateProfile'));
});

Route::group(array('prefix' => \Config::get('backend.admin_folder'), 'before' => 'auth-admin'), function(){
    Route::get('/', array('as' => 'admin-home', 'uses' => 'Backend\HomeController@frontpage'));
    Route::get('/users/list', array('as' => 'user-list', 'uses' => 'Backend\UsersController@users'));
});
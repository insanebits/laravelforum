<?php

class TopicVote extends Eloquent
{
    protected $table = 'topic_votes';

    protected $fillable = ['user_id', 'topic_id', 'vote'];
}
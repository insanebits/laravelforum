<?php

class Group extends Eloquent
{
    public $table = 'groups';

    public $timestamps = false;
}
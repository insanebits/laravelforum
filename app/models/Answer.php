<?php

class Answer extends Eloquent
{
    protected $table = 'answers';

    protected $fillable = ['content', 'topic_id', 'user_id'];

    public function author()
    {
        return $this->belongsTo('User', 'user_id', 'id');
    }

    public function upvotes()
    {
        return $this->votes()->where('vote', '1');
    }

    public function downvotes()
    {
        return $this->votes()->where('vote', '-1');
    }

    public function score()
    {
        $upvotes = $this->upvotes()->count();
        $downvotes = $this->downvotes()->count();

        return $upvotes - $downvotes;
    }

    public function votes()
    {
        return $this->hasMany('AnswerVote');
    }
}
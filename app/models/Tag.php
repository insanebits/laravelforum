<?php

class Tag extends Eloquent
{
    protected $table = 'tags';

    protected $fillable = ['title', 'slug'];

    public function topics()
    {
        return $this->hasManyThrough('Topic', 'TopicTag', 'tag_id', 'id');
    }
}
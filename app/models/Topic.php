<?php

class Topic extends Eloquent
{
    protected $table = 'topics';
    protected $fillable = ['title', 'content'];

    public function save(array $options = array())
    {
        if(!empty($this->title))
        {
            $this->slug = \Str::slug($this->title);
        }

        return parent::save($options);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany('Answer', 'topic_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag', 'topic_tags');
    }

    public function upvotes()
    {
        return $this->votes()->where('vote', '1');
    }

    public function downvotes()
    {
        return $this->votes()->where('vote', '-1');
    }

    public function score()
    {
        $upvotes = $this->upvotes()->count();
        $downvotes = $this->downvotes()->count();

        return $upvotes - $downvotes;
    }

    public function votes()
    {
        return $this->hasMany('TopicVote');
    }

    public function excerpt()
    {
        return substr($this->content, 0, 280);
    }

    public function author()
    {
        return $this->belongsTo('User', 'author_id');
    }
}
<?php

class AnswerVote extends Eloquent
{
    protected $table = 'answer_votes';

    protected $fillable = ['user_id', 'answer_id', 'vote'];
}
<?php

namespace Core\Lists;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Controller
{
    protected static function normalizeSettings($settings)
    {
        $defaults = array(
            'get_key' => 'list',
            'limit' => '10',
            'page_size' => 5,
            'page' => 1,
            'order_by' => '',
            'order_direction' => 'DESC',
            'columns' => array(),
            'layout' => 'backend.default.list', //todo should not be hardcoded
            'query' => false,
            'show_pk' => false,
            'bulk_actions' => array(
                'delete' => 1 // this way it's possible to disable default bulk actions
            ),
            'pagination_view' => \Config::get('view.pagination'),
            'view_data' => array()
        );

        $settings = $settings + $defaults;
        return $settings;
    }

    public static function make(\Eloquent $model, $settings = array())
    {
        $settings = self::normalizeSettings($settings);

        if(!empty($settings['columns']))
        {
            $columns = $settings['columns'];
        } else {
            throw new InvalidConfigurationException('Missing columns field');
        }

        // make sure every column is in same format
        $columns = self::normalizeColumns($columns, $settings['show_pk']);

        if(!$settings['query'])
        {
            $query = $model->newQuery();
        } else {
            $query = $settings['query'];
        }

        $query = static::applySearch($query, $columns);

        // apply limit
        $query->take($settings['limit']);
        //
        $query->offset((max(intval($settings['page']), 1) - 1) * $settings['limit']);

        if(!empty($settings['order_by']))
        {
            $query->orderBy($settings['order_by'], $settings['order_direction']);
        }

        $pagination = $query->paginate($settings['page_size']);

        // todo dynamic filtering;

        $rows = $query->get();

        // format cells
        $list = self::format(is_array($rows) ? $rows : $rows->toArray(), $columns);

        // remove disabled bulk actions
        foreach ($settings['bulk_actions'] as $k => $action) {
            if(!$action)
            {
                unset($settings['bulk_actions'][$k]);
            }
        }


        return \View::make($settings['layout'], array(
                'pagination' => $pagination->links($settings['pagination_view']),
                'list' => $list,
                'columns' => $columns,
                'bulk_actions' => $settings['bulk_actions']
            ) + $settings['view_data']);
    }

    protected static function applySearch($query, $columns)
    {
        $searchQuery = \Input::get('search');

        if($searchQuery)
        {
            $searchable = array();
            foreach ($columns as $col) {
                if($col['searchable'])
                {
                    $searchable[] = $col['name'];
                }
            }

            if(count($searchable) > 0)
            {
                $query->where(function($query) use($searchQuery, $searchable){
                    $col = array_shift($searchable);
                    $query->where($col, 'LIKE', '%' . $searchQuery . '%');

                    foreach($searchable as $col)
                    {
                        $query->orWhere($col, 'LIKE', '%' . $searchQuery . '%');
                    }
                });

            }

        }

        return $query;
    }

    protected static function format($list, $columns)
    {
        // go through every row in list
        foreach($list as &$row) {
            // make sure its array
            $row = (array)$row;
            foreach($columns as $col) {
                // column has user defined formatter, execute it
                if($col['format'] instanceof \Closure)
                {
                    $formatter = $col['format'];

                    // notice that $col['name'] might not exist in $item if user needs some custom column
                    $row[$col['name']] = $formatter($row, $col);
                }
            }
        }

        return $list;
    }

    protected static function humanize($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = str_replace('-', ' ', $string);

        $string = ucwords($string);

        return $string;
    }

    protected static function normalizeColumns($columns, $show_pk)
    {
        $result = array();

        $column_defaults = array(
            'format' => false,
            'visible' => true,
            'searchable' => false
        );

        foreach($columns as $setup) {
            if(is_string($setup))
            {
                $setup = array(
                        'label' => self::humanize($setup),
                        'name' => $setup,
                    ) + $column_defaults;
            } elseif(is_array($setup)) {
                $setup = $setup + $column_defaults;

                if(!isset($setup['label']))
                {
                    $setup['label'] = self::humanize($setup['name']);
                }
            }

            if($show_pk === false && $setup['name'] == 'id')
            {
                $setup['visible'] = false;
            }

            $result[] = $setup;
        }

        return $result;
    }

    /**
     * Will
     * @param $model
     * @param $settings
     * @param $data
     * @return bool
     */
    public static function processBulkActions($model, $settings, $data)
    {
        $handled = false;
        $settings = self::normalizeSettings($settings);

        if(isset($settings['bulk_actions']) && is_array($settings['bulk_actions']))
        {
            // search for bulk action in settings
            foreach ($settings['bulk_actions'] as $action_name => $action_enabled) {
                if($data['bulk_action'] == $action_name && $action_enabled)
                {
                    switch($action_name)
                    {
                        case 'delete' :
                            if(isset($data['bulk_selection']) && is_array($data['bulk_selection'])){
                                foreach ($data['bulk_selection'] as $selected) {
                                    $model->find($selected)->delete();
                                }
                                $handled = true;
                            }
                            break;

                        case 'delete_trashed' :
                            if(isset($data['bulk_selection']) && is_array($data['bulk_selection'])){
                                foreach ($data['bulk_selection'] as $selected) {
                                    $model->withTrashed()->find($selected)->forceDelete();
                                }
                                $handled = true;
                            }
                            break;

                        case 'restore':
                            if(isset($data['bulk_selection']) && is_array($data['bulk_selection'])){
                                foreach ($data['bulk_selection'] as $selected) {
                                    $model->onlyTrashed()->find($selected)->restore();
                                }

                                $handled = true;
                            }
                            break;
                    }
                }
            }
        }

        return $handled;
    }
}
<?php

namespace Backend;

use Core\Lists\Controller as ListController;

class UsersController extends BaseController
{
    public function users()
    {
        $this->layout->content = ListController::make(new \User(), array(
            'columns' => array(
                'firstname',
                'lastname',
                'username',
                'email',
            ),
            'view_data' => array(
                'list_title' => 'User list'
            )
        ));
    }
}
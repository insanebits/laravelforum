<?php

namespace Backend;

class BaseController extends \BaseController
{
    protected $templateDir = 'backend.';

    protected $defaultConfigFile = 'backend';

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        parent::setupLayout();

        // not the best solution, but otherwise each @extends statement would need @extends('master', ['title' => $title])
        // this is because variable will go to child view and not the master itself
        \View::share('title', $this->config('website_title'));
    }

    public function __construct()
    {
        // setup template directory
        $this->templateDir .= $this->config('template') . '.';

        parent::__construct();
    }


}
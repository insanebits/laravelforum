<?php

namespace Backend;

class HomeController extends BaseController
{
    public function frontpage()
    {
        $this->layout->content = \View::make($this->templateDir . 'home');
    }
}
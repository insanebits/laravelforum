<?php

namespace Frontend;

class LoginController extends BaseController
{
    public function login()
    {
        // handle submit
        if(\Input::isMethod('post'))
        {
            if(\Auth::attempt(array('username' => \Input::get('username'), 'password' => \Input::get('password'))))
            {
                return \Redirect::intended(route('user-profile'));
            } else {
                return \Redirect::back()->withInput()
                    ->withErrors(['auth-validation' => 'Invalid username or password']);
            }
        }

        // show form
        $this->layout->content = \View::make($this->templateDir . 'login');
        $this->layout->user_profile = ''; // disable guest header
    }

    public function logout()
    {
        \Auth::logout();

        return \Redirect::route('home');
    }

    public function register()
    {
        $formErrors = null;

        if(\Input::isMethod('post'))
        {
            $formData = \Input::all();
            $validationRules = array(
                'username' => 'required|min:3|unique:users,username',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|confirmed'
            );

            $validator = \Validator::make($formData, $validationRules);

            if($validator->passes())
            {
                $fields = array_keys($validationRules);
                unset($fields['password']);

                $userData = array();

                foreach($fields as $field) {
                    $userData[$field] = \Input::get($field);
                }

                $user = new \User($userData);

                $user->password = \Hash::make(\Input::get('password'));

                if($user->save())
                {
                    return \Redirect::route('login');
                }
            } else {
                $formErrors = $validator->errors();
            }
        }

        // show form
        $formView = \View::make('frontend/register')->with('errors', $formErrors);

        $this->layout->content = $formView;
        $this->layout->user_profile = ''; // disable guest header
    }
}
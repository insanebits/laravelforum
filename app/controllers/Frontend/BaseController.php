<?php

namespace Frontend;

class BaseController extends \BaseController
{
    protected $templateDir = 'frontend.';


    protected $defaultConfigFile = 'frontend';


    public function __construct()
    {
        // setup template directory
        $this->templateDir .= $this->config('template') . '.';

        parent::__construct();
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        parent::setupLayout();

        if(\Auth::check())
        {
            $this->layout->user_profile = \View::make($this->templateDir . '.user.header', array('user' => \Auth::user()));
        } else {
            $this->layout->user_profile = \View::make($this->templateDir . '.guest.header');
        }

        // not the best solution, but otherwise each @extends statement would need @extends('master', ['title' => $title])
        // this is because variable will go to child view and not the master itself
        \View::share('title', $this->config('website_title'));
    }
}
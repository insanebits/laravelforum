<?php

namespace Frontend;

class TopicController extends BaseController
{
    private $topicsPerPage = 20;

    private $createAction = 'topic-form';
    private $successRoute = 'home';

    public function latestTopics()
    {
        $topics = \Topic::take($this->topicsPerPage)
            ->with('upvotes')
            ->with('downvotes')
            ->get();

        $this->layout->content = \View::make($this->templateDir . 'topic.list', array(
            'topics' => $topics
        ));
    }

    public function createTopic()
    {
        if(\Input::isMethod('post'))
        {
            \Validator::extend('tags', function($attribute, $value, $parameters){
                $tags = explode(',', $value);

                // notice that we do not check if any tags are defined, 'required' rule is responsible for it
                foreach($tags as $tag)
                {
                    // has to start with letter or a number
                    if(!preg_match('/^[a-zA-Z0-9]+[a-zA-Z0-9-\'_ ]+$/ism', $tag))
                    {
                        return false;
                    }
                }

                return true;
            });

            $formData = array(
                'title' => \Input::get('name'),
                'content' => \Input::get('question'),
                'tags' => \Input::get('tags')
            );

            $validator = \Validator::make($formData, array(
                'title' => 'required|min:5',
                'content' => 'required|min:30',
                'tags' => 'required|tags'
            ));

            if($validator->fails())
            {
                return \Redirect::route($this->createAction)
                    ->withErrors($validator)
                    ->withInput();
            }

            $entity = new \Topic($formData);

            $entity->author_id = \Auth::user()->id;

            $entity->save();

            // list of tag ids to sync
            $topicTags = array();

            // create missing tags
            $tags = \Input::get('tags');

            foreach(explode(',', $tags) as $tag) {
                $tagModel = \Tag::where('title', $tag)->first();

                if(!$tagModel || !$tagModel->exists)
                {
                    $tagModel = new \Tag(array('title' => $tag, 'slug' => \Str::slug($tag)));
                    $tagModel->save();
                }

                $topicTags[] = $tagModel->id;
            }

            $entity->tags()->sync($topicTags);

            return \Redirect::route($this->successRoute);
        }

        $this->layout->content = \View::make($this->templateDir . 'topic.create');
    }

    public function showTopic($id, $slug)
    {
        $d = true;
        /** @var \Topic $topic */
        $topic = \Topic::where('id', $id)
            ->where('slug', $slug)
            ->first();

        if(!$topic->exists)
        {
            \App::abort(404);
        }

        // eager load additional data
        $answers = $topic->answers()
            ->with('author')
            ->with('upvotes')
            ->with('downvotes')

             ->get();

        $this->layout->content = \View::make($this->templateDir . 'topic.show')
            ->with('topic', $topic)
            ->with('answers', $answers);
    }

    public function answerTopic()
    {
        $formData = array(
            'content' => \Input::get('content'),
            'topic_id' => \Input::get('topic_id'),
            'user_id' => \Auth::user()->id
        );

        $validator = \Validator::make($formData, array(
            'topic_id' => 'exists:topics,id',
            'content' => 'required|min:30',
            'user_id' => 'exists:users,id',
        ));

        $topic = \Topic::findOrFail($formData['topic_id']);


        if($validator->fails())
        {
            return \Redirect::route('topic-view', array($topic->id, $topic->slug))
                ->withErrors($validator)
                ->withInput();
        }

        $entity = new \Answer($formData);

        $entity->save();

        // we can redirect to action without actually knowing the route itself, this way we can change route and app won't break
        return \Redirect::action('topic-view', array($topic->id, $topic->slug));
    }

    public function voteTopic($topic, $user, $value)
    {
        $voteData = array(
            'topic_id' => $topic->id,
            'user_id' => $user->id,
            'vote' => $value
        );

        $validator = \Validator::make($voteData, array(
            'topic_id' => 'exists:topics,id',
            'user_id' => 'exists:users,id',
            'vote' => 'in:1,-1'
        ));

        if($validator->fails())
        {
            return $validator->errors();
        }


        $topicVote = new \TopicVote($voteData);

        $topicVote->save();


        return true;
    }

    public function voteAnswer($answer, $user, $value)
    {
        $voteData = array(
            'answer_id' => $answer->id,
            'user_id' => $user->id,
            'vote' => $value
        );

        $validator = \Validator::make($voteData, array(
            'answer_id' => 'exists:answers,id',
            'user_id' => 'exists:users,id',
            'vote' => 'in:1,-1'
        ));

        if($validator->fails())
        {
            return $validator->errors();
        }


        $answerVote = new \AnswerVote($voteData);

        $answerVote->save();


        return true;
    }

    public function upvoteAnswer($answerId)
    {
        $answer = \Answer::find($answerId);

        if($answer->exists())
        {
            $userVoted = $answer->votes()->where('user_id', \Auth::user()->id)->first();

            if($userVoted) {
                $userVoted->vote = '1';
                $userVoted->save();
            } else {
                $this->voteAnswer($answer, \Auth::user(), '1');
            }
        }

        return \Redirect::back();
    }

    public function downvoteAnswer($answerId)
    {
        $answer = \Answer::find($answerId);

        if($answer->exists())
        {
            $userVoted = $answer->votes()->where('user_id', \Auth::user()->id)->first();

            if($userVoted) {
                $userVoted->vote = '-1';
                $userVoted->save();
            } else {
                $this->voteAnswer($answer, \Auth::user(), '1');
            }
        }

        return \Redirect::back();
    }

    public function upvoteTopic($topicId)
    {
        $topic = \Topic::find($topicId);

        if($topic->exists())
        {
            $userVoted = $topic->votes()->where('user_id', \Auth::user()->id)->first();

            if($userVoted) {
                $userVoted->vote = '1';
                $userVoted->save();
            } else {
                $this->voteTopic($topic, \Auth::user(), '1');
            }
        }

        return \Redirect::back();
    }

    public function downvoteTopic($topicId)
    {
        $topic = \Topic::find($topicId);

        if($topic->exists())
        {
            $userVoted = $topic->votes()->where('user_id', \Auth::user()->id)->first();

            if($userVoted) {
                $userVoted->vote = '-1';
                $userVoted->save();
            } else {
                $this->voteTopic($topic, \Auth::user(), '-1');
            }
        }

        return \Redirect::back();
    }
}
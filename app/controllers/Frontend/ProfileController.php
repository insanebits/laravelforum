<?php

namespace Frontend;

class ProfileController extends \Frontend\BaseController
{
    public function privateProfile()
    {
        $user = \Auth::user();

        echo \DynamicConfiguration::get('test');

        $this->layout->content = \View::make($this->templateDir . 'user.profile')
            ->with('user', $user);
    }
}
<?php

namespace Frontend;

class TagController extends BaseController
{
    protected $showTags = '50';
    protected $showTopics = '20';

    public function tagsList()
    {
        $this->layout->content = \View::make($this->templateDir . 'tags', array(
            'tags' => \Tag::take($this->showTags)->get()
        ));
    }

    public function showTag($slug)
    {
        /** @var \Tag $tag */
        $tag = \Tag::where('slug', $slug)
            ->first();

        if(!$tag->exists)
        {
            \App::abort(404);
        }

        // eager load additional data
        $topics = $tag->topics()->take($this->showTopics)->get();

        $this->layout->content = \View::make($this->templateDir . 'topic.list')
            ->with('topics', $topics)
            ->with('title', sprintf('Topics tagged with %s', $tag->title));
    }
}
<?php

abstract class BaseController extends Controller {

	/**
	 * Used when creating views to allow templating
	 * @var string
	 */
	protected $templateDir = 'default.';

	/**
	 * Where assets are located will depend on theme
	 *
	 * @var string
	 */
	protected $assetsDir = 'default/';

	/**
	 * Default configuration file used by config method
	 * @var string
	 */
	protected $defaultConfigFile = 'cms';

	/**
	 * Every theme does
	 * @var string
	 */
	protected $masterLayoutName = 'layout';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( !is_null($this->layout))
		{
			$this->layout = \View::make($this->layout);
		}
	}

	function __construct()
	{
		$this->layout = $this->templateDir . $this->masterLayoutName;
	}


	/**
	 *
	 * @param $key
	 * @param $config
	 * @return mixed
	 */
	protected function config($key, $prefix = '')
	{
		$path = (!empty($config) ? $config : $this->defaultConfigFile) . '.' . $key;
		return \Config::get($path);
	}

}
